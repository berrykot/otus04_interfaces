﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace otus04_interfaces.HomeWork.Tests
{
	[TestClass()]
	public class AccountServiceTests
	{
		[TestMethod()]
		public void AddAccountTest()
		{
			var mockAccService = new Mock<IAccountService>();

			mockAccService.Setup(accServ => accServ.AddAccount(It.IsAny<Account>()));

			var accountService = mockAccService.Object;

			foreach (var acc in new AccountGenerator())
			{
				accountService.AddAccount(acc);
			}

			mockAccService.Verify(accServ => accServ.AddAccount(It.IsAny<Account>()));
		}
	}
}