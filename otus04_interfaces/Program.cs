﻿using System;
using otus04_interfaces.StrategyPattern;
using otus04_interfaces.AbstractFactory;
using otus04_interfaces.DecoratorPattern;
using otus04_interfaces.CovarianceContravariance;
using System.Xml;
using otus04_interfaces.HomeWork;
using System.Linq;
using System.Text;

namespace otus04_interfaces
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			//1. Реализация IEnumerable на примере чтения списка элементов из xml файла
			var res1 = new Work1().Start();

			//2. Создать интерфейс IAlgorithm, добавить в него метод. Например, сортировка. Применить интерфейс к классу из первого задания.
			new Work2().Start(res1);

			//3. Реализуйте интерфейсы из https://gist.github.com/Veikedo/46e588fc1c52bbf03186597af23fcd61
			new Work3().Start();


			Console.ReadKey();
		}

	}

}
