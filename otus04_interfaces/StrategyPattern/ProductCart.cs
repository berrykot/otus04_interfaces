﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.StrategyPattern
{
	class ProductCart : List<Product>
	{
		public decimal GetCost()
		{
			decimal cost = 0;
			ForEach(p => cost += p.Price);
			return cost;
		}
	}
}
