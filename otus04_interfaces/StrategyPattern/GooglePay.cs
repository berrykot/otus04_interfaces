﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.StrategyPattern
{
	class GooglePay : IPay
	{
		public void Pay(decimal payAmount, Action<bool> afterPayCallback)
		{
			Console.WriteLine($"Подтвердите оплату в размере {payAmount} через Google Pay, введя в консоли 'Y'");
			var userAgree = Console.ReadLine().ToUpper() == "Y";
			if (userAgree)
			{
				Console.WriteLine($"Оплата через Google pay успешна");
				afterPayCallback?.Invoke(true);
			}
			else
			{
				Console.WriteLine($"Оплата через Google pay неуспешна");
				afterPayCallback?.Invoke(false);
			}
		}
	}
}
