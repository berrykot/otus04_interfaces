﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.StrategyPattern
{
	class Product
	{
		public string Name { get; private set; }
		public decimal Price { get; private set; }
		public Product(string name)
		{
			Name = name;
			Price = new Random(Name.GetHashCode()).Next(99, 999);
		}
	}
}
