﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.CovarianceContravariance
{
	class Person
	{
		public string Name { get; set; }
		public virtual void Display() => Console.WriteLine($"Person {Name}");
	}
	class Client : Person
	{
		public override void Display() => Console.WriteLine($"Client {Name}");
	}
}
