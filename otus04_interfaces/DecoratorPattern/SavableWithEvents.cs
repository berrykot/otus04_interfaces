﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.DecoratorPattern
{
	//Декоратор
	class SavableWithEvents : ISavable
	{
		private ISavable _savable;
		public event EventHandler OnSaving;
		public event EventHandler OnSaved;
		public SavableWithEvents(ISavable savable)
		{
			_savable = savable;
		}
		public void Save()
		{
			OnSaving?.Invoke(this, null);
			_savable.Save();
			OnSaved?.Invoke(this, null);
		}
	}
}
