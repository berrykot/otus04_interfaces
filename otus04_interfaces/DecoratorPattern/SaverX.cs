﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.DecoratorPattern
{
	class SaverX : ISavable
	{
		public void Save()
		{
			Console.WriteLine("Save");
		}
	}
}
