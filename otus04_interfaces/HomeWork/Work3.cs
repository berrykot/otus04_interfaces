﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.HomeWork
{
	class Work3
	{
		public void Start()
		{
			var repo = new AccountRepositoryCsv("repo.csv");

			var accServ = new AccountService(repo);

			foreach(var acc in new AccountGenerator())
			{
				try
				{
					accServ.AddAccount(acc);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}

		}

	}
}
