﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace otus04_interfaces.HomeWork
{
	//Задание 1:
	//Реализация IEnumerable на примере чтения списка элементов из xml файла
	class XmlEnumerable : IEnumerable<XmlNode>, IAlgorithm
	{
		private List<XmlNode> _nodes;
		public XmlEnumerable(XmlDocument doc)
		{
			_nodes = new List<XmlNode>();
			var docElement = doc?.DocumentElement ?? throw new ArgumentNullException(nameof(doc));
			foreach (XmlNode node in docElement)
			{
				_nodes.Add(node);
			}
		}
		public IEnumerator<XmlNode> GetEnumerator()
		{
			for (var i = 0; i < _nodes.Count; i++)
				yield return _nodes[i];
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void SortRandom()
		{
			var nodes = new List<XmlNode>();
			var rand = new Random(Guid.NewGuid().GetHashCode());
			var usedJs = new List<int>();
			int j = 0;
			for (var i = 0; i < _nodes.Count; i++)
			{
				do
				{
					j = rand.Next(0, _nodes.Count);
				}
				while (usedJs.Contains(j));
				usedJs.Add(j);
				nodes.Add(_nodes[j]);
			}
			_nodes = nodes;
		}
	}
}
