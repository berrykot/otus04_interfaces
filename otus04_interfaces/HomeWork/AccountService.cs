﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.HomeWork
{
	public class AccountService : IAccountService
	{
		public IRepository<Account> Repo { get; private set; }
		public AccountService(IRepository<Account> repo)
		{
			Repo = repo ?? throw new ArgumentNullException(nameof(repo));
		}
		public void AddAccount(Account account)
		{
			if (IsAccountValid(account))
				Repo.Add(account);
			else throw new ArgumentOutOfRangeException("Account", account, "Имя, фамилия и дата рождения должны быть заполнены, а возраст больше 18 лет");
		}
		public bool IsAccountValid(Account account)
		{
			var age = (new DateTime(1, 1, 1) + (DateTime.Now - account.BirthDate)).Year - 1;
			return age > 18
				&& account.BirthDate != default
				&& account.FirstName != null
				&& account.FirstName != string.Empty
				&& account.LastName != null
				&& account.LastName != string.Empty;
		}
	}
}
